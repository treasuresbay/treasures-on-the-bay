At Treasures on the Bay, our goal is to make life easy for you. If you are looking to rent in North Bay Village, take note of our stunning community of contemporary apartments for rent and the conveniences we offer. For example, our resident portal has been created exclusively for our residents.

Address: 7525 East Treasure Dr, North Bay Village, FL 33141

Phone: 305-866-4230